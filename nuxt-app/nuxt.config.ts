import { defineNuxtConfig } from 'nuxt'

// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
    css: [
        'quasar/dist/quasar.prod.css'
    ],
    ssr: false,
    target:"static",
})
